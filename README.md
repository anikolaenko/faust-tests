1) First of all this project is requires minimum Python 3.6 and Java 8+.

```
sudo apt-get install git python python3.7 python-pip python3-pip mc unzip default-jre
sudo pip3 install -U faust
```

2) Download this project on each worker and extract zip archive.

```
git clone https://anikolaenko@bitbucket.org/anikolaenko/faust-tests.git
```

Go to project folder:

```
cd faust-tests
```

3) Configure Kafka server. This need to be done on one worker machine. By default Zookeeper started on localhost and this is ok. But Kafka
need to be configured for run on public IP. In kafka/config edit server.properties

change

```
#listeners=PLAINTEXT://:9092
```

to public IP - KAFKA_IP

```
listeners=PLAINTEXT://KAFKA_IP:9092
```

4) Run Kafka + Zookeeper. On this worker run these commands from project folder:

```
./zookeeper/bin/zkServer.sh start
./kafka/bin/kafka-server-start.sh ./kafka/config/server.properties
```

Better to create one screen for zookeeper and one screen for Kafka. Later this will be simple toc heck for error or other messages.


5) Run Faust app on each worker machine:

5.1) Configure Faust worker
go to folder ./code and edit page_views.py In this script need to be set KAFKA_IP

5.2) Run Faust worker

```
faust -A page_views worker -l info
```

6) Now you can run tests

6.1) For check tests results, you can run Kafka consumer.

```
kafka/bin/kafka-console-consumer.sh --topic page_views-page_views-changelog --bootstrap-server KAFKA_IP:9092 --property print.key=True --from-beginning
```

This command can print any updates in page_views topic.

6.2) Send test events, from ./code folder run this command few times:

```
faust -A page_views send page_views '{"id": "foo", "user": "bar"}'
```

6.3) Check kafka consumer console, and should be displayed:
foo: 1
foo: 2
foo: 3


7) Prepare for other tests. For run other tests, Kafka package need to be installed:

```
pip install kafka-python
```

In folder ./code edit file send_event.py, use KAFKA_IP in "bootstrap_servers" parameter and try to run it.

```
python send_event.py
```

8) For my tests I tried to run multiple tests, for example i tried to send Kafka event 1M times, due this execution
i tried to kill all workers, start few times... etc. But as result, after run workers again in Kafka was registered proper events count - 1M.

I tried different events and in all cases i wasn't able to broke it.


9) Setup multiple brokers. Start zookeeper on each worker and configure Kafka for use multiple zookeepers. In Kafka on each worker

kafka/config/server.properties

change zookeeper.connect property, for example:

```
zookeeper.connect=KAFKA_IP_1:2181,KAFKA_IP_2:2181,KAFKA_IP_3:2181
```

run Kafka on each worker.

Now we can create topic:

```
bin/kafka-topics.sh --create --zookeeper KAFKA_IP_1:2181,KAFKA_IP_2:2181,KAFKA_IP_3:2181 --replication-factor 3 --partitions 2 --topic topic3brokers
```

Check if everything looks good:

```
bin/kafka-topics.sh --describe --zookeeper KAFKA_IP_1:2181,KAFKA_IP_2:2181,KAFKA_IP_3:2181 --topic topic3brokers
Topic:topic3brokers	PartitionCount:2	ReplicationFactor:3	Configs:
	Topic: topic3brokers	Partition: 0	Leader: 2	Replicas: 2,1,0	Isr: 2,1,0
	Topic: topic3brokers	Partition: 1	Leader: 0	Replicas: 0,1,2	Isr: 0,1,2
```
Number of partitions in topic can be changed with this command:

```
bin/kafka-topics.sh --zookeeper KAFKA_IP_1:2181,KAFKA_IP_2:2181,KAFKA_IP_3:2181 --alter --topic topic3brokers --partitions 4
```

10) Run Python Kafka clients. You can run multiple Kafka consumers, number of Consumers should be same as partitions count
in topic.

Consumer example receive_event.py:
```
from kafka import KafkaConsumer

consumer = KafkaConsumer('topic3brokers',
                         group_id='my-group',
                         bootstrap_servers='KAFKA_IP_1:9092,KAFKA_IP_2:9092,KAFKA_IP_3:9092',
                         auto_offset_reset='earliest', enable_auto_commit=True
                         )
for message in consumer:
    # for unicode: `message.value.decode('utf-8')`
    print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                          message.offset, message.key,
                                          message.value))

    # save number in db or kafka topic
    # save number in different Kafka topic
```

For test you can run two Kafka consumers in different consoles (python receive_event.py).

Now you can send some new events. In this script we will send 10 events as example:

```
python send_event.py
```

In first consumer console you will see:
```
topic3brokers:0:1774: key=page_1 value={"url": "http://some.url/1", "id": "page_1"}
topic3brokers:0:1775: key=page_2 value={"url": "http://some.url/2", "id": "page_2"}
topic3brokers:0:1776: key=page_4 value={"url": "http://some.url/4", "id": "page_4"}
topic3brokers:0:1777: key=page_5 value={"url": "http://some.url/5", "id": "page_5"}
topic3brokers:0:1778: key=page_7 value={"url": "http://some.url/7", "id": "page_7"}
topic3brokers:0:1779: key=page_8 value={"url": "http://some.url/8", "id": "page_8"}
```

In second consumer console you will see:
```
topic3brokers:1:30: key=page_0 value={"url": "http://some.url/0", "id": "page_0"}
topic3brokers:1:31: key=page_3 value={"url": "http://some.url/3", "id": "page_3"}
topic3brokers:1:32: key=page_6 value={"url": "http://some.url/6", "id": "page_6"}
topic3brokers:1:33: key=page_9 value={"url": "http://some.url/9", "id": "page_9"}
```


11) Enable SSL in Kafka brokers

First of all we need to generate server and client certificates using Java keytool utility and sign these certificates:

```
# create server cert
keytool -keystore server.keystore.jks -alias localhost -validity 365 -keyalg RSA -genkey

# create client cert
openssl req -new -x509 -keyout ca-key -out ca-cert -days 365
keytool -keystore server.truststore.jks -alias CARoot -import -file ca-cert
keytool -keystore client.truststore.jks -alias CARoot -import -file ca-cert

# create your own CA and sign certificates
keytool -keystore server.keystore.jks -alias localhost -certreq -file cert-file
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days 365 -CAcreateserial -passin pass:111111
keytool -keystore server.keystore.jks -alias CARoot -import -file ca-cert
keytool -keystore server.keystore.jks -alias localhost -import -file cert-signed
```

111111 - this is default password


As next step we can configure Kafka brokers. In kafka/config edit server.properties

change

#listeners=PLAINTEXT://:9092

to public IP - KAFKA_IP

listeners=SSL://KAFKA_IP:9092

and add these lines with path to certificates:

```
ssl.keystore.location=/var/private/ssl/server.keystore.jks
ssl.keystore.password=111111
ssl.key.password=111111
ssl.truststore.location=/var/private/ssl/server.truststore.jks
ssl.truststore.password=111111

security.inter.broker.protocol=SSL
```

Now we can run Kafka broker:

```
./kafka/bin/kafka-server-start.sh ./kafka/config/server.properties
```

In case of errors, these will be shown in Kafka console. To check if if the server keystore and truststore are
setup properly we can use following command:

```
openssl s_client -debug -connect KAFKA_IP:9092 -tls1
```

12) Enable SSL in Kafka clients

For Kafka SSL clients we need to create configuration file (for example client-ssl.properties) with following content:

```
security.protocol=SSL
ssl.truststore.location=/var/private/ssl/client.truststore.jks
ssl.truststore.password=111111
ssl.keystore.location=/var/private/ssl/server.keystore.jks
ssl.keystore.password=111111
ssl.key.password=111111
```

Console Consumer example:
```
kafka/bin/kafka-console-consumer.sh --topic topic_local --bootstrap-server localhost:9092 --property print.key=True --from-beginning --consumer.config /var/private/client-ssl.properties
```

Python Producer and Consumer examples:


First of all we need to prepare certificates and keys for python client:

```
keytool -exportcert -alias localhost -keystore server.keystore.jks -rfc -file certificate.pem
```

```
keytool -v -importkeystore -srckeystore server.keystore.jks -srcalias localhost -destkeystore cert_and_key.p12 -deststoretype PKCS12
```

save output key of this command in key.pem

```
openssl pkcs12 -in cert_and_key.p12 -nocerts -nodes
```

```
keytool -exportcert -alias CARoot -keystore server.keystore.jks -rfc -file CARoot.pem
```


Now we can update python code and test:

```
producer = KafkaProducer(
    bootstrap_servers='localhost:9092',
    value_serializer=lambda m: json.dumps(m).encode('ascii'),
    security_protocol='SSL',
    ssl_check_hostname=False,
    ssl_cafile='/var/private/ssl/CARoot.pem',
    ssl_certfile='/var/private/ssl/certificate.pem',
    ssl_keyfile='/var/private/ssl/key.pem',
    ssl_password='111111'
)
```

Consumer SSL implementations will be same, for example:

```
consumer = KafkaConsumer(
    'topic3brokers',
    group_id='my-group',
    bootstrap_servers='KAFKA_IP_1:9092,KAFKA_IP_2:9092,KAFKA_IP_3:9092',
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    security_protocol='SSL',
    ssl_check_hostname=False,
    ssl_cafile='/var/private/ssl/CARoot.pem',
    ssl_certfile='/var/private/ssl/certificate.pem',
    ssl_keyfile='/var/private/ssl/key.pem',
    ssl_password='111111'
                         )
for message in consumer:
    # for unicode: `message.value.decode('utf-8')`
    print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                          message.offset, message.key,
                                          message.value))
```

If you want to set ssl_check_hostname parameter to true:

```
ssl_check_hostname=True
```

on each worker need to be generated separate client cert where hostname equals to client IP address. This will enable connection
to Kafka only from this IP address.