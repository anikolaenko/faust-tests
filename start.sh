#!/bin/bash

# ZooKeeper
./zookeeper/bin/zkServer.sh start &

# Kafka
./kafka/bin/kafka-server-start.sh ./kafka/config/server.properties &
