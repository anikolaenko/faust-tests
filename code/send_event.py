# producer implementation

from kafka import KafkaProducer
from kafka.errors import KafkaError

import json
import datetime, calendar

def get_utc_timestamp():
  return calendar.timegm(datetime.datetime.utcnow().utctimetuple())


producer = KafkaProducer(
    bootstrap_servers='104.248.64.150:9092,157.230.153.11:9092,165.22.130.41:9092',
    value_serializer=lambda m: json.dumps(m).encode('ascii'),
    #security_protocol='SSL',
    #ssl_check_hostname=False,
    #ssl_cafile='/var/private/ssl/CARoot.pem',
    #ssl_certfile='/var/private/ssl/certificate.pem',
    #ssl_keyfile='/var/private/ssl/key.pem',
    #ssl_password='111111'
)

start_time = get_utc_timestamp()
for i in range(10):
    res = producer.send('topic3brokers', {"id": "page_%s" % i, "url": "http://some.url/%s" % i}, "page_%s" % i)

producer.flush()
print("Time: %s" % (get_utc_timestamp() - start_time))