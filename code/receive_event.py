# consumer implementation
#
#   pip install kafka-python
#   pip install pymemcache

import json, pprint
import pymongo
from pymemcache.client import base

from kafka import KafkaConsumer


consumer = KafkaConsumer('topic3brokers',
                         group_id='my-group',
                         bootstrap_servers='104.248.64.150:9092,157.230.153.11:9092,165.22.130.41:9092',
                         enable_auto_commit=True,
                         auto_offset_reset='earliest',
                         #security_protocol='SSL',
                         #ssl_check_hostname=False,
                         #ssl_cafile='/var/private/ssl/CARoot.pem',
                         #ssl_certfile='/var/private/ssl/certificate.pem',
                         #ssl_keyfile='/var/private/ssl/key.pem',
                         #ssl_password='111111'
                         )
for message in consumer:
    # for unicode: `message.value.decode('utf-8')`
    print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                          message.offset, message.key,
                                          message.value))
    try:
        payload = json.loads(message.value)

        # save number in db or kafka topic, for example in mongodb
        client = pymongo.MongoClient('localhost').topic3brokers
        client.page_views.update({'url': payload['url']}, {'$inc': {'count': 1}}, upsert=True)

        # save number in redis or memcached as example below
        client = base.Client(('localhost', 11211))
        value = client.get(payload['url'])

        if not value:
            value = 1

        client.set(payload['url'], int(value) + 1)

    except Exception as e:
        print("Problem in input data", e)
